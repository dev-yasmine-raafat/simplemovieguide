package com.yasmineraafat.myfirstkotlinapp

import com.google.gson.annotations.SerializedName

class MoviesResult {

    @SerializedName("results") var results:List<MovieModel> = ArrayList()
}