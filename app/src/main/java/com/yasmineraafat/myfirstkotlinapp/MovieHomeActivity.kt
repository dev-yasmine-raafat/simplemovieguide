package com.yasmineraafat.myfirstkotlinapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_movie_home.*

class MovieHomeActivity : AppCompatActivity(), ItemClickListener{

    val language:String = "en-US"
    val apiKey: String = "6781264da246aa3b52e3fe6982ecd371"

    var movieAdapter: MovieRecyclerAdapter? = null

    var disposable: Disposable? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_home)

        val categoryList = ArrayList<String>()
        categoryList.add("Now playing")
        categoryList.add("Popular")
        categoryList.add("Top rated")
        categoryList.add("Upcoming")
        categoryList.add("Latest")


        recyclerViewCats.adapter = CatsRecyclerAdapter(categoryList,this,this)
        recyclerViewCats.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL,false)

        movieAdapter = MovieRecyclerAdapter(ArrayList(), this)
        recyclerViewMovies.adapter = movieAdapter
        recyclerViewMovies.layoutManager = LinearLayoutManager(this)

        showNowPlayingMovies()

    }

    private fun showLatestMovies() {

        disposable = WebServiceClient.getClient().getLatest(apiKey,language)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            loadData(result.results)
                        },
                        { error -> error(error.message) }
                )

    }

    private fun loadData(items: List<MovieModel>) {
        progressBar.visibility = View.GONE
        recyclerViewMovies.visibility = View.VISIBLE
        movieAdapter?.items = items
        movieAdapter?.notifyDataSetChanged()

        if (items.isEmpty())
            error("No movies to show")
    }

    private fun error(msg: String?) {
        progressBar.visibility = View.GONE
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
    }

    private fun showNowPlayingMovies() {

        disposable = WebServiceClient.getClient().getNowPlaying(apiKey,language)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            loadData(result.results)
                        },
                        { error -> error(error.message) }
                )

    }

    private fun showPopularMovies() {

        disposable = WebServiceClient.getClient().getPopular(apiKey,language)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            loadData(result.results)
                        },
                        { error -> error(error.message) }
                )

    }

    private fun showTopRatedMovies() {

        disposable = WebServiceClient.getClient().getTopRated(apiKey,language)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            loadData(result.results)
                        },
                        { error -> error(error.message) }
                )

    }

    private fun showUpcomingMovies() {

        disposable = WebServiceClient.getClient().getUpcoming(apiKey,language)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            loadData(result.results)
                        },
                        { error -> error(error.message) }
                )

    }

    override fun itemClick(i: Int) {


        progressBar.visibility = View.VISIBLE
        recyclerViewMovies.visibility = View.GONE

        when(i){
            0 -> showNowPlayingMovies()
            1 -> showPopularMovies()
            2 -> showTopRatedMovies()
            3 -> showUpcomingMovies()
            4 -> showLatestMovies()
        }
    }

}
