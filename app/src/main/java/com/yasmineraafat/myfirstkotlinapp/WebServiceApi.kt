package com.yasmineraafat.myfirstkotlinapp

import io.reactivex.Observable
import okhttp3.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface WebServiceApi{
    @GET("movie/upcoming")
    fun getUpcoming(@Query("api_key") apiKey: String,
                    @Query("language") language:String): Observable<MoviesResult>

    @GET("movie/latest")
    fun getLatest(@Query("api_key") apiKey: String,
                  @Query("language") language:String): Observable<MoviesResult>

    @GET("movie/now_playing")
    fun getNowPlaying(@Query("api_key") apiKey: String,
                      @Query("language") language:String): Observable<MoviesResult>

    @GET("movie/popular")
    fun getPopular(@Query("api_key") apiKey: String,
                   @Query("language") language:String): Observable<MoviesResult>

    @GET("movie/top_rated")
    fun getTopRated(@Query("api_key") apiKey: String,
                    @Query("language") language:String): Observable<MoviesResult>
}