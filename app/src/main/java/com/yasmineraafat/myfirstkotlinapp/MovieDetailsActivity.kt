package com.yasmineraafat.myfirstkotlinapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_movie_details.*

class MovieDetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_details)

        val movie = Gson().fromJson(intent.getStringExtra("movie"), MovieModel::class.java)
        Picasso.with(this).load(WebServiceClient.baseUrl+movie.posterPath).into(ivPoster)
        tvTitle?.text = movie.title
        tvRate?.text = movie.voteAverage.toString()+"/10"
        tvReleaseDate?.text = movie.releaseDate
        tvOverview?.text = movie.overview
    }
}

