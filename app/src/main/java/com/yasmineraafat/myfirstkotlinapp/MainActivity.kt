package com.yasmineraafat.myfirstkotlinapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {


    private lateinit var calc: Calculator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        calc = Calculator()

        btnPlus.setOnClickListener(this)
        btnMinus.setOnClickListener(this)
        btnDivision.setOnClickListener(this)
        btnMultiply.setOnClickListener(this)

    }

    override fun onClick(view: View?) {

        when(view){

            btnPlus -> tvResult.text = calc.sum(etNum1.text.toString().toInt(),etNum2.text.toString().toInt()).toString()
            btnMinus -> tvResult.text =calc.sub(etNum1.text.toString().toInt(),etNum2.text.toString().toInt()).toString()
            btnDivision -> tvResult.text =calc.div(etNum1.text.toString().toInt(),etNum2.text.toString().toInt()).toString()
            btnMultiply -> tvResult.text =calc.multiply(etNum1.text.toString().toInt(),etNum2.text.toString().toInt()).toString()

        }

    }

    class Calculator {
        fun sum(num1:Int,num2:Int):Int{

            return num1+num2
        }
        fun sub(num1:Int,num2:Int):Int{

            return num1-num2
        }
        fun div(num1:Int,num2:Int):Int{

            return num1/num2
        }
        fun multiply(num1:Int,num2:Int):Int{

            return num1*num2
        }
    }


}
