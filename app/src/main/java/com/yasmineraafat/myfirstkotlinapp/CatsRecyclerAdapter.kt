package com.yasmineraafat.myfirstkotlinapp

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.list_item_cats.view.*

class CatsRecyclerAdapter (var items : List<String>, val context: Context, val itemListener:ItemClickListener) :
        RecyclerView.Adapter<CatsViewHolder>(){

    // Gets the number of animals in the list
    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CatsViewHolder {
        return CatsViewHolder(LayoutInflater.from(context).inflate(R.layout.list_item_cats, parent, false))
    }

    override fun onBindViewHolder(holder: CatsViewHolder, position: Int) {
        holder.btnCat?.text = items.get(position)

        holder.btnCat.setOnClickListener { itemListener.itemClick(position) }
    }
}

class CatsViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    // Holds the TextView that will add each animal to
    val btnCat = view.btnCat


}