package com.yasmineraafat.myfirstkotlinapp

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_item_movie_type.view.*


class MovieRecyclerAdapter(var items : List<MovieModel>, val context: Context) : RecyclerView.Adapter<ViewHolder>() {

    // Gets the number of animals in the list
    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_item_movie_type, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Picasso.with(context).load(WebServiceClient.baseUrl+items[position].posterPath).into(holder.ivPoster)
        Log.e("image",WebServiceClient.baseUrl+items[position].posterPath)
        holder.tvTitle?.text = items[position].title
        holder.tvRate?.text = items[position].voteAverage.toString()+"/10"
        holder.tvReleaseDate?.text = items[position].releaseDate

        holder.btnDetails.setOnClickListener {
            val intent = Intent(context, MovieDetailsActivity::class.java)
            intent.putExtra("movie",Gson().toJson(items[position]))
            context.startActivity(intent)
        }
    }
}

class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    // Holds the TextView that will add each animal to
    val ivPoster = view.ivPoster
    val tvTitle = view.tvTitle
    val tvRate = view.tvRate
    val tvReleaseDate = view.tvReleaseDate
    val btnDetails = view.btnDetails
}